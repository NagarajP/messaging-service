/**
 * 
 */
package com.myzee.messagingservice;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author nagar
 *
 */

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {
	
}
