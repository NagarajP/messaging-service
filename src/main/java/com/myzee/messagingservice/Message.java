package com.myzee.messagingservice;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
//@Data
//@AllArgsConstructor
public class Message {
	
	@Id
	@GeneratedValue
	private int messageId;
	private String message;
	private Date date;
	private String status;
	
	public Message() {
		// TODO Auto-generated constructor stub
	}
	
	public Message(String message, Date date, String status) {
		super();
		this.message = message;
		this.date = date;
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Message [message=" + message + ", date=" + date + ", status=" + status + "]";
	}
	
	
	
}
