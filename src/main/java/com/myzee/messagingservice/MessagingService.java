package com.myzee.messagingservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/msg")
public class MessagingService {
	
	private static Logger logger = LogManager.getLogger(MessagingService.class);
	@Autowired
	MessageRepository messageRepository;
	
	@PostMapping(value = "/send", produces = "application/json")
	public String sendMessage(@RequestBody Message msg) {
		msg.setDate(new Date());
		msg.setStatus("Success");
		messageRepository.save(msg);
		
		System.out.println("message sent is - " + msg.toString());
		logger.info(msg.toString());
		String result = "Message sent successfully - " + msg.getMessage() + " on " + msg.getDate();
		return result;
	}
	
	@GetMapping(value = "/getlist")
	public MessageList getMassageList() {
		List<Message> msgList = new ArrayList<Message>();
		/*msgList.add(new Message("message 1", new Date(), "success"));
		msgList.add(new Message("message 2", new Date(), "failed"));
		msgList.add(new Message("message 3", new Date(), "success"));
		*/
		
		Iterator<Message> itr =  messageRepository.findAll().iterator();
		MessageList ml = new MessageList();
		while (itr.hasNext()) {
			Message message = (Message) itr.next();
			msgList.add(message);
		}
		
		ml.setMsgList(msgList);
		return ml;
	}
	
}
