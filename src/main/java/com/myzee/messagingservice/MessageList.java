package com.myzee.messagingservice;

import java.util.List;
import com.myzee.messagingservice.*;

public class MessageList {

	private List<Message> msgList;
	
	public List<Message> getMsgList() {
		return msgList;
	}
	
	public void setMsgList(List<Message> msgList) {
		this.msgList = msgList;
	}
}
